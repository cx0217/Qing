﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qing.Lang;
using Qing.Proto;

namespace Qing.Std {
    public class StdInit {
        /*定义好的原生函数，通过初始化方法添加到语句中*/
        public static void InitStd(Ctx ctx) {

            ctx.PutNow("@退出", new Expr(new Quite()));
            ctx.PutNow("@显示", new Expr(new Print()));
            ctx.PutNow("@获取输入", new Expr(new Ask()));
            ctx.PutNow("@垃圾回收", new Expr(new JGC()));
            ctx.PutNow("@清屏", new Expr(new Clear()));
            ctx.PutNow("@调用命令", new Expr(new CallCmd()));

            ctx.PutNow("@关于", new Expr(new About()));
            ctx.PutNow("@版本", new Expr(new Version()));

            //ctx.PutNow("@取反", new Expr(new Anti()));
            ctx.PutNow("@删除", new Expr(new Del()));
            ctx.PutNow("@复制", new Expr(new Copy()));
            ctx.PutNow("@拷贝", new Expr(new Clone()));
            ctx.PutNow("@执行", new Expr(new Eval()));
            ctx.PutNow("@取类型", new Expr(new TypeOf()));
            ctx.PutNow("@内部类型", new Expr(new RawType()));
            ctx.PutNow("@包含", new Expr(new Include()));
            ctx.PutNow("@引入", new Expr(new Import()));
            ctx.PutNow("@加载动态库", new Expr(new LoadDll()));
            //ctx.PutNow("@运行原生脚本", new Expr(new RunCSharpScript()));

            ctx.PutNow("@分支", new Expr(new Parallel()));
            ctx.PutNow("@并行", new Expr(new Spawn()));
            ctx.PutNow("@并行锁", new Expr(new Lock()));

            ctx.PutNow("@当前目录", new Expr(new Pwd()));
            ctx.PutNow("@文件是否存在", new Expr(new Exists()));
            ctx.PutNow("@读二进制文件", new Expr(new ReadBinFile()));
            ctx.PutNow("@读文本文件", new Expr(new ReadTextFile()));
            ctx.PutNow("@写二进制文件", new Expr(new WriteBinFile()));
            ctx.PutNow("@写文本文件", new Expr(new WriteTextFile()));
            ctx.PutNow("@文件重命名", new Expr(new RenameFile()));
            ctx.PutNow("@移动文件", new Expr(new MoveFile()));
            ctx.PutNow("@复制文件", new Expr(new CopyFile()));
            ctx.PutNow("@删除文件", new Expr(new DeleteFile()));
            ctx.PutNow("@切换目录", new Expr(new Fcd()));
            ctx.PutNow("@创建目录", new Expr(new Fdir()));
            ctx.PutNow("@目录列表", new Expr(new Fls()));
            ctx.PutNow("@文件绝对路径", new Expr(new Fabs()));

            ctx.PutNow("@计时", new Expr(new Cost()));
            ctx.PutNow("@系统时间", new Expr(new Now()));
            ctx.PutNow("@休眠", new Expr(new Sleep()));
            ctx.PutNow("@定时任务", new Expr(new Timer()));
            ctx.PutNow("@启动定时任务", new Expr(new TimerStart()));
            ctx.PutNow("@停止定时任务", new Expr(new TimerStop()));
            ctx.PutNow("@延时任务", new Expr(new TimeoutTask()));
            ctx.PutNow("@等待任务", new Expr(new WaitTask()));

            ctx.PutNow("@创建定时器", new Expr(new CreateTimer()));

            ctx.PutNow("@取长度", new Expr(new Length()));
            ctx.PutNow("@追加", new Expr(new Append()));
            ctx.PutNow("@插入", new Expr(new Insert()));
            ctx.PutNow("@取出", new Expr(new Take()));
            ctx.PutNow("@切片", new Expr(new TakeSub()));
            ctx.PutNow("@替换", new Expr(new Replace()));
            ctx.PutNow("@查找", new Expr(new Find()));
            ctx.PutNow("@分割", new Expr(new Split()));
            ctx.PutNow("@裁切", new Expr(new Trim()));
            ctx.PutNow("@范围", new Expr(new Range()));

            ctx.PutNow("@创建缓冲字符串", new Expr(new StrBuilder()));
            ctx.PutNow("@起于", new Expr(new StrStartsWith()));
            ctx.PutNow("@止于", new Expr(new StrEndsWith()));

            ctx.PutNow("@创建哈希表", new Expr(new CreateHashMapObj()));
            ctx.PutNow("@创建哈希集", new Expr(new CreateHashSetObj()));

            ctx.PutNow("@转换", new Expr(new Cast()));
            ctx.PutNow("@保留小数", new Expr(new DecimalRound()));
            ctx.PutNow("@解码字符串", new Expr(new DecodeStr()));
            ctx.PutNow("@编码字符串", new Expr(new EncodeStr()));

            ctx.PutNow("@转Json", new Expr(new ToJson()));
            ctx.PutNow("@解析Json", new Expr(new ParseJson()));

            
            Expr math = new Expr(TP.Obj, new Obj());
            math.Obj().PutNow("@绝对值", new Expr(new MathAbs()));
            math.Obj().PutNow("@最大值", new Expr(new MathMax()));
            math.Obj().PutNow("@最小值", new Expr(new MathMin()));
            math.Obj().PutNow("@向下取整", new Expr(new MathFloor()));
            math.Obj().PutNow("@向上取整", new Expr(new MathCeil()));
            math.Obj().PutNow("@四舍五入", new Expr(new MathRound()));
            math.Obj().PutNow("@乘方", new Expr(new MathPow()));
            math.Obj().PutNow("@平方根", new Expr(new MathSqrt()));
            math.Obj().PutNow("@随机数", new Expr(new MathRandom()));
            math.Obj().PutNow("@随机数种子", new Expr(new MathRandomSeek()));
            math.Obj().PutNow("#PI", new Expr(TP.Float, (decimal)Math.PI));
            math.Obj().PutNow("@tan", new Expr(new MathTan()));
            math.Obj().PutNow("@sin", new Expr(new MathSin()));
            math.Obj().PutNow("@cos", new Expr(new MathCos()));
            math.Obj().PutNow("@atan", new Expr(new MathAtan()));
            math.Obj().PutNow("@asin", new Expr(new MathAsin()));
            math.Obj().PutNow("@acos", new Expr(new MathAcos()));
            ctx.PutNow("#数学库", math);

            Expr date = new Expr(TP.Obj, new Obj());
            date.Obj().PutNow("@系统时间", new Expr(new DateNow()));
            date.Obj().PutNow("@格式化", new Expr(new DateFormat()));
            date.Obj().PutNow("@创建", new Expr(new DateCreate()));
            date.Obj().PutNow("@时间戳", new Expr(new DateStamp()));
            date.Obj().PutNow("@毫秒数", new Expr(new DateMill()));
            date.Obj().PutNow("@取年份", new Expr(new DateGetYear()));
            date.Obj().PutNow("@取月份", new Expr(new DateGetMonth()));
            date.Obj().PutNow("@取日期", new Expr(new DateGetDay()));
            date.Obj().PutNow("@取周几", new Expr(new DateGetDayOfWeek()));
            date.Obj().PutNow("@取年内日数", new Expr(new DateGetDayOfYear()));
            date.Obj().PutNow("@取小时", new Expr(new DateGetHours()));
            date.Obj().PutNow("@取分钟", new Expr(new DateGetMinutes()));
            date.Obj().PutNow("@取秒数", new Expr(new DateGetSecond()));
            date.Obj().PutNow("@取毫秒", new Expr(new DateGetMill()));
            date.Obj().PutNow("@取时钟", new Expr(new DateGetTicks()));
            date.Obj().PutNow("@设年份", new Expr(new DateSetYear()));
            date.Obj().PutNow("@设月份", new Expr(new DateSetMonth()));
            date.Obj().PutNow("@设日期", new Expr(new DateSetDay()));
            date.Obj().PutNow("@设小时", new Expr(new DateSetHours()));
            date.Obj().PutNow("@设分钟", new Expr(new DateSetMinutes()));
            date.Obj().PutNow("@设秒数", new Expr(new DateSetSecond()));
            date.Obj().PutNow("@设毫秒", new Expr(new DateSetMill()));
            ctx.PutNow("#时间库", date);



            ctx.PutNow("@创建套接字", new Expr(new CreatePort()));
            ctx.PutNow("@http请求", new Expr(new HttpRequest()));

        }
    }
}
