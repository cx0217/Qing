﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Qing.Lang;


namespace Qing.Std {


    class DateNow : Native {
        public override string Name { get; set; } = "系统时间";
        public override string Desc { get; set; } = "弃用，无参，返回对象；获取系统当前时间";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            return new Expr(TP.Date, DateTime.Now);
        }

    }

    class DateFormat : Native {
        public override string Name { get; set; } = "格式化";
        public override string Desc { get; set; } = "弃用，参数1-时间，参数2-字符串，返回字符串；获取时间格式化字符串";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            if(args.Count == 0) {
                return Expr.Err("时间·格式化函数参数不足");
            }
            if (args[0].Tp == TP.Date) {
                if (args.Count == 1 ) {
                    return new Expr(TP.Str, args[0].Date().ToString());
                }else if (args.Count > 1 && args[1].Tp == TP.Str) {
                    return new Expr(TP.Str, args[0].Date().ToString(args[1].Str()));
                }

            }

            return Expr.Err("时间·格式化函数参数错误");
        }

    }

    class DateCreate : Native {
        public override string Name { get; set; } = "创建";
        public override string Desc { get; set; } = "弃用，参数1-字符串，返回时间；通过字符串创建时间";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            
            if (args.Count == 0) {
                return new Expr(TP.Date, new DateTime());
            } else if (args.Count > 0 && args[0].Tp == TP.Str){
                return new Expr(TP.Date, DateTime.Parse(args[0].Str()));
            }

            return Expr.Err("时间·创建函数参数错误");
        }

    }

    class DateStamp : Native {
        public override string Name { get; set; } = "时间戳";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回字符串；返回时间对应的时间戳";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区

                long timeStamp = (long)(args[0].Date() - startTime).TotalMilliseconds; // 相差毫秒数

                return new Expr(TP.Str, timeStamp.ToString());
            }

            return Expr.Err("时间·时间戳函数参数错误");
        }

    }

    class DateMill : Native {
        public override string Name { get; set; } = "毫秒数";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回整数；获取时间对应的毫秒数";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(2000, 1, 1)); // 当地时区

                long timeStamp = (long)(args[0].Date() - startTime).TotalMilliseconds; // 相差毫秒数

                return new Expr(TP.Int, (int)timeStamp);
            }

            return Expr.Err("时间·毫秒数函数参数错误");
        }

    }

    class DateGetYear : Native {
        public override string Name { get; set; } = "取年份";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回整数；获取时间的年份数";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                return new Expr(TP.Int, args[0].Date().Year);
            }

            return Expr.Err("时间·取年份函数参数错误");
        }

    }

    class DateGetMonth : Native {
        public override string Name { get; set; } = "取月份";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回整数；获取时间的月份数";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                return new Expr(TP.Int, args[0].Date().Month + 1);
            }

            return Expr.Err("时间·取月份函数参数错误");
        }

    }

    class DateGetDay : Native {
        public override string Name { get; set; } = "取日期";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回整数；获取时间的日期";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                return new Expr(TP.Int, args[0].Date().Day);
            }

            return Expr.Err("时间·取日期函数参数错误");
        }

    }

    class DateGetDayOfWeek : Native {
        public override string Name { get; set; } = "取周几";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回整数；获取时间的周几";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                return new Expr(TP.Int, args[0].Date().DayOfWeek);
            }

            return Expr.Err("时间·取周几函数参数错误");
        }

    }

    class DateGetDayOfYear : Native {
        public override string Name { get; set; } = "取年内日数";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回整数；获取时间的年内日数";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                return new Expr(TP.Int, args[0].Date().DayOfYear);
            }

            return Expr.Err("时间·取年内日数函数参数错误");
        }

    }

    class DateGetHours : Native {
        public override string Name { get; set; } = "取小时";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回整数；获取时间的小时数";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                return new Expr(TP.Int, args[0].Date().Hour);
            }

            return Expr.Err("时间·取小时函数参数错误");
        }

    }

    class DateGetMinutes : Native {
        public override string Name { get; set; } = "取分钟";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回整数；获取时间的分钟数";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                return new Expr(TP.Int, args[0].Date().Minute);
            }

            return Expr.Err("时间·取分钟函数参数错误");
        }

    }

    class DateGetSecond : Native {
        public override string Name { get; set; } = "取秒数";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回整数；获取时间的秒数";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                return new Expr(TP.Int, args[0].Date().Second);
            }

            return Expr.Err("时间·取秒数函数参数错误");
        }

    }

    class DateGetMill : Native {
        public override string Name { get; set; } = "取毫秒";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回整数；获取时间的毫秒数";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                return new Expr(TP.Int, args[0].Date().Millisecond);
            }

            return Expr.Err("时间·取毫秒函数参数错误");
        }

    }

    class DateGetTicks : Native {
        public override string Name { get; set; } = "取时钟";
        public override string Desc { get; set; } = "弃用，参数1-时间，返回字符串；获取时间的时钟";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 0 && args[0].Tp == TP.Date) {
                return new Expr(TP.Str, args[0].Date().Ticks.ToString());
            }

            return Expr.Err("时间·取时钟函数参数错误");
        }

    }


    class DateSetYear : Native {
        public override string Name { get; set; } = "设年份";
        public override string Desc { get; set; } = "弃用，参数1-时间，参数2-整数，返回参数1；设置参数1时间的年份";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 1 && args[0].Tp == TP.Date && args[1].Tp == TP.Int) {
                DateTime dt = args[0].Date();
                string dtStr = args[1].Int().ToString() + "-";
                dtStr += dt.ToString("MM-dd hh:mm:ss.fff");
                args[0].Val = DateTime.Parse(dtStr);
                return args[0];
            }

            return Expr.Err("时间·取时钟函数参数错误");
        }

    }

    class DateSetMonth : Native {
        public override string Name { get; set; } = "设月份";
        public override string Desc { get; set; } = "弃用，参数1-时间，参数2-整数，返回参数1；设置参数1时间的月份";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 1 && args[0].Tp == TP.Date && args[1].Tp == TP.Int) {
                DateTime dt = args[0].Date();
                string dtStr = dt.ToString("yyyy-");
                dtStr += args[1].Int().ToString();
                dtStr += dt.ToString("-dd hh:mm:ss.fff");
                args[0].Val = DateTime.Parse(dtStr);
                return args[0];
            }

            return Expr.Err("时间·设月份函数参数错误");
        }

    }


    class DateSetDay : Native {
        public override string Name { get; set; } = "设日期";
        public override string Desc { get; set; } = "弃用，参数1-时间，参数2-整数，返回参数1；设置参数1时间的日期";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 1 && args[0].Tp == TP.Date && args[1].Tp == TP.Int) {
                DateTime dt = args[0].Date();
                string dtStr = dt.ToString("yyyy-MM-");
                dtStr += args[1].Int().ToString() + " ";
                dtStr += dt.ToString("hh:mm:ss.fff");
                args[0].Val = DateTime.Parse(dtStr);
                return args[0];
            }

            return Expr.Err("时间·设日期函数参数错误");
        }

    }

    class DateSetHours : Native {
        public override string Name { get; set; } = "设小时";
        public override string Desc { get; set; } = "弃用，参数1-时间，参数2-整数，返回参数1；设置参数1时间的小时";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 1 && args[0].Tp == TP.Date && args[1].Tp == TP.Int) {
                DateTime dt = args[0].Date();
                string dtStr = dt.ToString("yyyy-MM-dd");
                dtStr += " " + args[1].Int().ToString();
                dtStr += dt.ToString(":mm:ss.fff");
                args[0].Val = DateTime.Parse(dtStr);
                return args[0];
            }

            return Expr.Err("时间·设小时函数参数错误");
        }

    }

    class DateSetMinutes : Native {
        public override string Name { get; set; } = "设分钟";
        public override string Desc { get; set; } = "弃用，参数1-时间，参数2-整数，返回参数1；设置参数1时间的分钟";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 1 && args[0].Tp == TP.Date && args[1].Tp == TP.Int) {
                DateTime dt = args[0].Date();
                string dtStr = dt.ToString("yyyy-MM-dd hh:");
                dtStr += args[1].Int().ToString() + ":";
                dtStr += dt.ToString("ss.fff");
                args[0].Val = DateTime.Parse(dtStr);
                return args[0];
            }

            return Expr.Err("时间·设分钟函数参数错误");
        }

    }


    class DateSetSecond : Native {
        public override string Name { get; set; } = "设秒数";
        public override string Desc { get; set; } = "弃用，参数1-时间，参数2-整数，返回参数1；设置参数1时间的秒数";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 1 && args[0].Tp == TP.Date && args[1].Tp == TP.Int) {
                DateTime dt = args[0].Date();
                string dtStr = dt.ToString("yyyy-MM-dd hh:mm:");
                dtStr += args[1].Int().ToString() + ".";
                dtStr += dt.ToString("fff");
                args[0].Val = DateTime.Parse(dtStr);
                return args[0];
            }

            return Expr.Err("时间·设秒数函数参数错误");
        }

    }

    class DateSetMill : Native {
        public override string Name { get; set; } = "设毫秒";
        public override string Desc { get; set; } = "弃用，参数1-时间，参数2-整数，返回参数1；设置参数1时间的毫秒数";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {

            if (args.Count > 1 && args[0].Tp == TP.Date && args[1].Tp == TP.Int) {
                DateTime dt = args[0].Date();
                string dtStr = dt.ToString("yyyy-MM-dd hh:mm:ss");
                dtStr += "." + args[1].Int().ToString();
                args[0].Val = DateTime.Parse(dtStr);
                return args[0];
            }

            return Expr.Err("时间·设毫秒函数参数错误");
        }

    }




}
