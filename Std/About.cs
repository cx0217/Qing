﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Qing.Lang;

namespace Qing.Std {

    class About : Native {
        public override string Name { get; set; } = "关于";
        public override string Desc { get; set; } = "无参，返回空；显示项目贡献者信息。";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            string info = @"青语言由以下贡献者开发：
**柳州市柳江区数心开物软件工作室（原创开发），联系邮箱：qingyuyan@aliyun.com 。**
";
            Console.WriteLine(info);
            return Expr.NoneExpr;
        }

    }

    class Version : Native {
        public override string Name { get; set; } = "版本";
        public override string Desc { get; set; } = "无参，返回字符串；显示当前版本信息。";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj = null, List<Expr>? namedArgs = null) {
            return new Expr(TP.Str, Env.Version);
        }

    }


}
