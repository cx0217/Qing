﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Qing.Lang;

namespace Qing.Std {

    class Length : Native {
        public override string Name { get; set; } = "取长度";
        public override string Desc { get; set; } = "参数1-字符串|数组|对象|二进制，返回整数；获取第一个参数的长度";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            if (args.Count == 0) {
                return Expr.Err("取长度函数参数不足");
            }
            Expr ret = new Expr(TP.Int, 0);
            if (args[0].Tp == TP.Str) {
                ret.Val = args[0].Str().Length;
                return ret;
            }
            if (args[0].Tp == TP.Bin) {
                ret.Val = args[0].Bin().Length;
                return ret;
            }
            if (args[0].Tp == TP.Arr || args[0].Tp == TP.Paren ) {
                ret.Val = args[0].List!.Count;
                return ret;
            }

            if (args[0].Tp == TP.Obj) {
                ret.Val = args[0].Ctx().Map.Count;
                return ret;
            }

            return Expr.Err("取长度函数参数错误");
        }

    }


    class Append : Native {
        public override string Name { get; set; } = "追加";
        public override string Desc { get; set; } = "参数1-字符串|数组，参数2-任意，可选参数3-逻辑，返回参数1；对于字符串会在末尾追加参数2转换为字符串的值，对于数组会把参数2追加到数组末尾。注意，如果参数1和参数2都是数组，那么会把参数2中的每一个值追加到参数1数组的末尾，如果需要把数组展开为多个值进行追加，需要再传入一个逻辑真";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            if (args.Count < 2) {
                return Expr.Err("追加函数参数不足");
            }
            bool expand = false;
            if (args.Count > 2) {
                expand = args[2].ToBool();
            }

            if (args[0].Tp == TP.Arr) {
                if (args[1].Tp == TP.Arr) {
                    if (!expand) {
                        args[0].List!.Add(args[1].Dup());
                    } else {
                        foreach (var item in args[1].List!) {
                            args[0].List!.Add(item.Dup());
                        }                     
                    }
                } else {
                    args[0].List!.Add(args[1].Dup());
                }
                return args[0];
            }

            if (args[0].Tp == TP.Str) {
                if ((args[1].Tp == TP.Str || args[1].Tp == TP.Rune) && !expand) {
                    args[0].Val = args[0].Str() + args[1].Str();
                    return args[0];
                }
                args[0].Val = args[0].Str() + args[1].ToStr();
                return args[0];
            }

            if (args[0].Tp == TP.Str && args[1].Tp == TP.Bin) {
                args[0].Val = args[0].Bin().Concat(args[1].Bin());
                return args[0];
            }

            return Expr.Err("追加函数参数错误");
        }

    }

    class Insert : Native {
        public override string Name { get; set; } = "插入";
        public override string Desc { get; set; } = "参数1-数组|字符串|二进制，参数2-整数，参数3-任意，可选参数4-逻辑，返回参数1；参数1为集合，根据参数2指定的位置，将参数3插入到参数1中，参数4默认为假，若为真则将参数3展开为多个值";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            if (args.Count < 3) {
                return Expr.Err("插入函数参数不足");
            }

            bool expand = false;
            if (args.Count > 3) {
                expand = args[3].ToBool();
            }

            if (args[0].Tp == TP.Arr && args[1].Tp == TP.Int) {
                int idx = args[1].Int();
                List<Expr> list = args[0].List!;
                if(idx < 0 || idx > list.Count + 0) {
                    return Expr.Err("插入函数的位置参数越界");
                }
                if (args[2].Tp == TP.Arr && expand) {
                    int i = 0;
                    while (i<args[2].List!.Count) {
                        list.Insert(idx+i, args[2].List![i].Dup());
                        i++;
                    }
                } else {
                    list.Insert(idx, args[2].Dup());
                }
                return args[0];
            }

            if (args[0].Tp == TP.Str && args[1].Tp == TP.Int) {
                string str = args[0].Str();
                int idx = args[1].Int();
                if (idx < 0 || idx > str.Length) {
                    return Expr.Err("插入函数的位置参数越界");
                }
                string target;
                if (!expand && (args[2].Tp == TP.Str || args[2].Tp == TP.Rune)) {
                    target = args[2].Str();
                } else {
                    target = args[2].ToStr();
                }
                
                if(idx == 0) {
                    args[0].Val = target + str;
                }else if (idx == str.Length){
                    args[0].Val = str + target;
                } else {
                    args[0].Val = str[0..idx] + target + str[idx..];
                }

                return args[0];
            }

            if (args[0].Tp == TP.Bin && args[1].Tp == TP.Int) {
                byte[] bts = args[0].Bin();
                int idx = args[1].Int();
                if (idx < 0 || idx > bts.Length) {
                    return Expr.Err("插入函数的位置参数越界");
                }
                byte[] target;
                if (args[2].Tp == TP.Bin ) {
                    target = args[2].Bin();
                } else {
                    return Expr.Err("插入函数的参数错误");
                }

                if (idx == 0) {
                    args[0].Val = target.Concat(bts);
                } else if (idx == bts.Length ) {
                    args[0].Val = bts.Concat(target);
                } else {
                    args[0].Val = bts[0..idx].Concat(target).Concat(bts[idx..]);
                }

                return args[0];
            }

            if (args[0].Tp == TP.Obj && args[1].Tp == TP.Str) {
                args[0].Ctx().PutNow(args[1].Str(), args[2].Dup());
                return args[0];
            }

            return Expr.Err("插入函数参数错误");
        }

    }


    class Take : Native {
        public override string Name { get; set; } = "取出";
        public override string Desc { get; set; } = "参数1-字符串|数组|二进制，参数2-整数，可选参数3-整数，返回任意；参数1为集合，从参数2指定的位置中取出1个元素，如果传入参数3，那么取出参数3指定个数的元素";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            if (args.Count < 2) {
                return Expr.Err("取出函数参数不足");
            }

            if (args[0].Tp == TP.Arr && args[1].Tp == TP.Int) {
                int len = 1;
                if (args.Count > 2 && args[2].Tp == TP.Int) {
                    len = args[2].Int();
                }
                if (args[1].Int() < 0 || args[1].Int() + len > args[0].List!.Count) {
                    return Expr.Err("取出函数的范围参数越界");
                }
                if(len == 1) {
                    Expr one = args[0].List![args[1].Int()];
                    args[0].List!.RemoveAt(args[1].Int());
                    return one;
                }
                Expr ret = new Expr(TP.Arr, 0, new List<Expr>());
                for(int i=0; i<len; i++) {
                    ret.List!.Add(args[0].List![args[1].Int()]);
                    args[0].List!.RemoveAt(args[1].Int());
                }
                return ret;
            }

            if (args[0].Tp == TP.Str && args[1].Tp == TP.Int) {
                string str = args[0].Str();
                int idx = args[1].Int();
                int len = 1;
                if (args.Count > 2 && args[2].Tp == TP.Int) {
                    len = args[2].Int();
                }
                if (args[1].Int() < 0 || idx + len > str.Length) {
                    return Expr.Err("取出函数的范围参数越界");
                }
/*                if(len == 1) {
                    Expr one = new Expr(TP.Rune, str[(idx-1)..idx]);
                    args[0].val = str[0..(idx-1)] + str[idx..];
                    return one;
                }*/
                Expr ret = new Expr(TP.Str, str[idx..(idx+len)]);
                args[0].Val = str[0..idx] + str[(idx+len)..];
                return ret;
            }

            if (args[0].Tp == TP.Bin && args[1].Tp == TP.Int) {
                byte[] bts = args[0].Bin();
                int idx = args[1].Int();
                int len = 1;
                if (args.Count > 2 && args[2].Tp == TP.Int) {
                    len = args[2].Int();
                }
                if (args[1].Int() < 0 || idx + len > bts.Length) {
                    return Expr.Err("取出函数的范围参数越界");
                }
                if (len == 1) {
                    Expr one = new Expr(TP.Bin, bts[idx..idx]);
                    args[0].Val = bts[0..idx].Concat(bts[(idx+1)..]);
                    return one;
                }
                Expr ret = new Expr(TP.Str, bts[idx..(idx+len)]);
                args[0].Val = bts[0..idx].Concat(bts[(idx+len)..]);
                return ret;
            }

            if (args[0].Tp == TP.Obj && args[1].Tp == TP.Str) {
                return args[0].Ctx().RemoveNow(args[1].Str());
            }

            return Expr.Err("取出函数参数错误");
        }

    }

    class TakeSub : Native {
        public override string Name { get; set; } = "切片";
        public override string Desc { get; set; } = "参数1-字符串|数组|二进制，参数2-整数，可选参数3-整数，返回字符串|数组|二进制；参数1为集合，以参数2为起始位置，参数3为截取长度（默认为1），截取集合片段";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            if (args.Count < 2) {
                return Expr.Err("切片函数参数不足");
            }

            if (args[0].Tp == TP.Arr && args[1].Tp == TP.Int) {
                int len = 1;
                if (args.Count > 2 && args[2].Tp == TP.Int) {
                    len = args[2].Int();
                }
                if (args[1].Int() < 0 || args[1].Int() + len > args[0].List!.Count) {
                    return Expr.Err("切片函数的范围参数越界");
                }
                if (len == 1) {
                    Expr one = args[0].List![args[1].Int()].Dup();
                    return one;
                }
                Expr ret = new Expr(TP.Arr, 0, new List<Expr>());
                for (int i = 0; i<len; i++) {
                    ret.List!.Add(args[0].List![args[1].Int() + i].Dup());
                }
                return ret;
            }

            if (args[0].Tp == TP.Str && args[1].Tp == TP.Int) {
                string str = args[0].Str();
                int idx = args[1].Int();
                int len = 1;
                if (args.Count > 2 && args[2].Tp == TP.Int) {
                    len = args[2].Int();
                }
                if (args[1].Int() < 0 || idx + len > str.Length ) {
                    return Expr.Err("切片函数的范围参数越界");
                }
/*                if (len == 1) {
                    Expr one = new Expr(TP.Rune, str[(idx-1)..idx]);
                    return one;
                }*/
                Expr ret = new Expr(TP.Str, str[idx..(idx+len)]);
                return ret;
            }

            if (args[0].Tp == TP.Bin && args[1].Tp == TP.Int) {
                byte[] bts = args[0].Bin();
                int idx = args[1].Int();
                int len = 1;
                if (args.Count > 2 && args[2].Tp == TP.Int) {
                    len = args[2].Int();
                }
                if (args[1].Int() < 0 || idx + len > bts.Length ) {
                    return Expr.Err("切片函数的范围参数越界");
                }
                if (len == 1) {
                    Expr one = new Expr(TP.Rune, bts[idx..(idx+1)]);
                    return one;
                }
                Expr ret = new Expr(TP.Str, bts[idx..(idx+len)]);
                return ret;
            }

            return Expr.Err("切片函数参数错误");
        }

    }

    class Replace : Native {
        public override string Name { get; set; } = "替换";
        public override string Desc { get; set; } = "参数1-字符串|数组，参数2-任意，参数3-任意，可选参数4-整数，返回参数1；参数1为集合，参数2为替换的旧值，参数3为替换的新值，参数4为替换的次数，默认全部替换";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            if (args.Count < 3 ) {
                return Expr.Err("替换函数参数不足");
            }
            
            int count = int.MaxValue;
            if(args.Count > 3 && args[3].Tp == TP.Int && args[3].Int() > 0) {
                count = args[3].Int();
            }

            bool only = false;
            if (args.Count > 4) {
                only = args[4].ToBool();
            }

            if (args[0].Tp == TP.Arr) {
                if (args[1].Tp == TP.Arr) {
                    int i = 0;
                    List<Expr> source = args[0].List!;
                    List<Expr> target = args[1].List!;

                    while(i < source.Count - target.Count + 1 && count > 0) {
                        bool match = true;
                        for(int j=0; j<target.Count; j++) {
                            if (!source[i+j].Eq(target[j])){
                                match = false;
                                break;
                            }
                        }
                        if (match) {
                            if(!only && args[2].Tp == TP.Arr) {
                                source.RemoveRange(i, target.Count);
                                for(int x=0; x<args[2].List!.Count; x++) {
                                    source.Insert(i, args[2].List![x].Dup());
                                    i++;
                                }
                            } else {
                                source.RemoveRange(i, target.Count);
                                source.Insert(i,args[2].Dup());
                            }

                            count--;

                        }
                        i++;
                    }

                    return args[0];
                }else {
                    int i = 0;
                    List<Expr> source = args[0].List!;
                    while(i < source.Count && count > 0) {
                        if (source[i].Eq(args[1])) {
                            if (!only && args[2].Tp == TP.Arr) {
                                List<Expr> target = args[2].List!;
                                source.RemoveAt(i);
                                for (int x = 0; x<args[2].List!.Count; x++) {
                                    source.Insert(i, args[2].List![x].Dup());
                                    i++;
                                }
                            } else {
                                source.RemoveAt(i);
                                source.Add(args[2].Dup());
                            }

                            count--;
                        }
                        i++;
                    }

                    return args[0];
                }

            }

            if (args[0].Tp == TP.Str) {
                string str = args[0].Str();

                string target;
                if (args[1].Tp == TP.Str || args[1].Tp == TP.Rune) {
                    target = args[1].Str();
                } else {
                    target = args[1].ToStr();
                }

                string newSub;
                if (!only && (args[2].Tp == TP.Str || args[2].Tp == TP.Rune)) {
                    newSub = args[2].Str();
                } else {
                    newSub = args[2].ToStr();
                }

                int i = 0;
                while(i<str.Length - target.Length + 1 && count > 0) {
                    if (str[i..(i+target.Length)] == target) {
                        str = str.Remove(i, target.Length);
                        str = str.Insert(i, newSub);
                        i+= newSub.Length;
                        count--;
                        continue;
                    }
                    i++;
                }
                args[0].Val = str;
                return args[0];
            }

            return Expr.Err("替换函数参数错误");
        }

    }

    class Find : Native {
        public override string Name { get; set; } = "查找";
        public override string Desc { get; set; } = "参数1-字符串|数组，参数2-任意，可选参数3-整数，返回整数；从参数1中查找参数2的值，返回查找到的位置，如果传入参数3，则作为查找的起始位置";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            if (args.Count < 2) {
                return Expr.Err("查找函数参数不足");
            }

            int nth = 1;
            if (args.Count > 2 && args[2].Tp == TP.Int && args[2].Int() > 0) {
                nth = args[2].Int();
            }

            if (args[0].Tp == TP.Arr) {
                if (args[1].Tp == TP.Arr) {
                    int i = 0;
                    List<Expr> source = args[0].List!;
                    List<Expr> target = args[1].List!;

                    while (i < source.Count - target.Count + 1 && nth > 0) {
                        bool match = true;
                        for (int j = 0; j<target.Count; j++) {
                            if (!source[i+j].Eq(target[j])) {
                                match = false;
                                break;
                            }
                        }
                        if (match) {
                            nth--;
                            if(nth == 0) {
                                return new Expr(TP.Int, i);
                            } else {
                                i+= target.Count;
                                continue;
                            }

                        }
                        i++;
                    }

                    return new Expr(TP.Int, -1);
                } else {
                    int i = 0;
                    List<Expr> source = args[0].List!;
                    while (i < source.Count && nth > 0) {
                        if (source[i].Eq(args[1])) {
                            nth--;
                            if (nth == 0) {
                                return new Expr(TP.Int, i);
                            }
                        }
                        i++;
                    }

                    return new Expr(TP.Int, -1);
                }

            }

            if (args[0].Tp == TP.Str) {
                string str = args[0].Str();

                string target;
                if (args[1].Tp == TP.Str || args[1].Tp == TP.Rune) {
                    target = args[1].Str();
                } else {
                    target = args[1].ToStr();
                }

                int i = 0;
                while (i<str.Length - target.Length + 1 && nth > 0) {
                    if (str[i..(i+target.Length)] == target) { 
                        nth--;
                        if (nth == 0) {
                            return new Expr(TP.Int, i);
                        } else {
                            i += target.Length;
                            continue;
                        }
                    }
                    i++;
                }

                return new Expr(TP.Int, -1);
            }

            return Expr.Err("查找函数参数错误");
        }

    }


    class Split : Native {
        public override string Name { get; set; } = "分割";
        public override string Desc { get; set; } = "参数1-字符串|数组，参数2-任意，返回数组；根据参数2，将参数1分割为多个片段";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            if (args.Count < 2) {
                return Expr.Err("分割函数参数不足");
            }

            if (args[0].Tp == TP.Arr) {
                Expr ret = new Expr(TP.Arr, 0, new List<Expr>());
                
                if (args[1].Tp == TP.Arr) {
                    int idx = 0;
                    int startIdx = 0;
                    List<Expr> source = args[0].List!;
                    List<Expr> target = args[1].List!;
                    if(source.Count == 0 || target.Count == 0) {
                        return args[0].Clone();
                    }
                    while(idx < source.Count - target.Count + 1) {
                        bool match = true;
                        for(int j=0; j<target.Count; j++) {
                            if (!source[idx+j].Eq(target[j])) {
                                match = false;
                                break;
                            }
                        }

                        if (match) {
                            Expr item = new Expr(TP.Arr,0, new List<Expr>());
                            for(int x=startIdx; x<idx; x++) {
                                item.List!.Add(source[x].Clone());
                            }
                            ret.List!.Add(item);
                            idx += target.Count;
                            startIdx = idx;
                            continue;
                        }

                        idx++;
                    }
                    if(startIdx < source.Count) {
                        Expr item = new Expr(TP.Arr, 0, new List<Expr>());
                        for (int x = startIdx; x<source.Count; x++) {
                            item.List!.Add(source[x].Clone());
                        }
                        ret.List!.Add(item);
                    }

                    return ret;
                } else {
                    int idx = 0;
                    int startIdx = 0;
                    List<Expr> source = args[0].List!;
                    Expr target = args[1];
                    while (idx < source.Count) {
                        if (source[idx].Eq(target)) { 
                            Expr item = new Expr(TP.Arr, 0, new List<Expr>());
                            for (int x = startIdx; x<idx; x++) {
                                item.List!.Add(source[x].Clone());
                            }
                            ret.List!.Add(item);                           
                            startIdx = idx+1;
                        }

                        idx++;
                    }

                    if (startIdx < source.Count) {
                        Expr item = new Expr(TP.Arr, 0, new List<Expr>());
                        for (int x = startIdx; x<source.Count; x++) {
                            item.List!.Add(source[x].Clone());
                        }
                        ret.List!.Add(item);
                    }

                    return ret;

                }


            } else if (args[0].Tp == TP.Str) {
                if (args[1].Tp == TP.Str || args[1].Tp == TP.Rune) {
                    string[] ss = args[0].Str().Split(args[1].Str());
                    Expr ret = new Expr(TP.Arr, 0, new List<Expr>());
                    foreach(var s in ss) {
                        ret.List!.Add(new Expr(TP.Str, s));
                    }
                    return ret;
                }
            
            }

            return Expr.Err("分割函数参数错误");
        }

    }


    class Trim : Native {
        public override string Name { get; set; } = "裁切";
        public override string Desc { get; set; } = "参数1-字符串|数组，可选参数2-整数，可选参数3-任意，返回参数1；参数1为集合，默认全裁切，参数2为0时做全裁切，参数2小于0时做左裁切，参数2大于0时做右裁切，参数3指定裁切的目标，默认为空值或空白字符";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            if (args.Count < 1) {
                return Expr.Err("裁切函数参数不足");
            }

            int mode = 0;
            if(args.Count > 1 && args[1].Tp == TP.Int) {
                mode = args[1].Int();
            }

            if (args[0].Tp == TP.Arr) {
                List<Expr> source = args[0].List!;
                Expr target = new Expr();
                if (args.Count > 2) {
                    target = args[2];
                }
                int startIdx = 0;
                int endIdx = source.Count - 1;

                if(mode < 0 || mode == 0) {
                    while(startIdx < source.Count) {
                        if (!source[startIdx].Eq(target)) {
                            break;
                        }
                        startIdx++;
                    }
                }

                if(mode > 0 || mode == 0) {
                    while (endIdx > 0) {
                        if (!source[endIdx].Eq(target)) {
                            break;
                        }
                        endIdx--;
                    }
                }

                if(startIdx > endIdx) {
                    args[0].List = new List<Expr>();
                } else {
                    args[0].List = source.GetRange(startIdx, endIdx-startIdx+1);
                }
                

                return args[0];

            } else if (args[0].Tp == TP.Str) {
                if(args.Count > 2 && args[2].Tp == TP.Str && args[2].Str().Length == 1 ) {
                    if (mode < 0) {
                        return new Expr(TP.Str, args[0].Str().TrimStart(args[2].Str()[0]));
                    } else if (mode == 0) {
                        return new Expr(TP.Str, args[0].Str().Trim(args[2].Str()[0]));
                    } else {
                        return new Expr(TP.Str, args[0].Str().TrimEnd(args[2].Str()[0]));
                    }
                }

                if(mode < 0) {
                    args[0].Val = args[0].Str().TrimStart();
                }else if(mode == 0) {
                    args[0].Val = args[0].Str().Trim();
                } else {
                    args[0].Val = args[0].Str().TrimEnd();
                }

                return args[0];
            }

            return Expr.Err("裁切函数参数错误");
        }

    }



    class Range : Native {
        public override string Name { get; set; } = "范围";
        public override string Desc { get; set; } = "参数1-整数|小数，可选参数2-整数|小数，可选参数3-整数|小数，返回数组；创建一个数组，根据传入的参数控制范围";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj = null, List<Expr>? namedArgs = null) {
            if (args.Count == 0) {
                return Expr.Err("范围函数参数不足");
            }
            List<Expr> list = new List<Expr>();
            Expr res = new Expr(TP.Arr, 0, list);

            if (args.Count == 1 && args[0].Tp == TP.Int) {
                int end = args[0].Int();
                if(end > 0) {
                    for(int i=0; i < end; i++) {
                        list.Add(new Expr(TP.Int, i));
                    }
                } else if(end <0) {
                    for (int i = 0; i > end; i--) {
                        list.Add(new Expr(TP.Int, i));
                    }
                }

                return res;
            }

            decimal step = 1;
            if(args.Count > 2 && (args[2].Tp == TP.Int || args[2].Tp == TP.Float)) {
                if (args[2].Tp == TP.Int) {
                    step = args[2].Int();
                } else {
                    step = args[2].Float();
                }
            }

            if (args.Count >= 2 && (args[0].Tp == TP.Int || args[0].Tp == TP.Float) && (args[1].Tp == TP.Int || args[1].Tp == TP.Float)) {
                decimal end = 0;
                if (args[1].Tp == TP.Int) {
                    end = args[1].Int();
                }else {
                    end = args[1].Float();
                }

                if(Op.Lt(new List<Expr> { args[0], args[1] }, ctx).ToBool()) {
                    if (args[0].Tp == TP.Int) {
                        if((int)step == step) {
                            for (int i = args[0].Int(); i <= end; i+=(int)step) {
                                list.Add(new Expr(TP.Int, i));
                            }
                        } else {
                            for (decimal f = args[0].Int(); f <= end; f+=step) {
                                list.Add(new Expr(TP.Float, f));
                            }
                        }
                        
                    } else {
                        for (decimal f = args[0].Float(); f <= end; f+=step) {
                            list.Add(new Expr(TP.Float, f));
                        }
                    }

                }else if(Op.Gt(new List<Expr> { args[0], args[1] }, ctx).ToBool()) {
                    if (args[0].Tp == TP.Int) {
                        if((int)step == step) {
                            for (int i = args[0].Int(); i >= end; i-=(int)step) {
                                list.Add(new Expr(TP.Int, i));
                            }
                        } else {
                            for (decimal f = args[0].Int(); f >= end; f-=step) {
                                list.Add(new Expr(TP.Float, f));
                            }
                        }
                        
                    } else {
                        for (decimal f = args[0].Float(); f >= end; f-=1) {
                            list.Add(new Expr(TP.Float, f));
                        }
                    }
                }
                return res;
            }


            return Expr.Err("范围函数参数错误");
        }

    }





}
