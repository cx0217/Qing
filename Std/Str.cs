﻿using Qing.Lang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qing.Proto;

namespace Qing.Std {


    class StrStartsWith : Native {
        public override string Name { get; set; } = "起于";
        public override string Desc { get; set; } = "参数1-字符串，参数2-字符串，返回逻辑象；判断参数1字符串是否以参数2字符串开头";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj = null, List<Expr>? namedArgs = null) {
            if(args.Count < 2 || args[0].Tp != TP.Str || args[1].Tp != TP.Str) {
                return Expr.Err("@起于 函数参数错误");
            }

            return new Expr(TP.Bool, args[0].Str().StartsWith(args[1].Str()));

        }

    }

    class StrEndsWith : Native {
        public override string Name { get; set; } = "止于";
        public override string Desc { get; set; } = "参数1-字符串，参数2-字符串，返回逻辑象；判断参数1字符串是否以参数2字符串结尾";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj = null, List<Expr>? namedArgs = null) {
            if (args.Count < 2 || args[0].Tp != TP.Str || args[1].Tp != TP.Str) {
                return Expr.Err("@止于 函数参数错误");
            }

            return new Expr(TP.Bool, args[0].Str().EndsWith(args[1].Str()));

        }

    }



}
