﻿using Qing.Lang;
using Qing.Std;
using Qing.Ext;
using Qing.Script;
using System.IO;


namespace Qing {
    public class Program {
        static void Main(string[] args) {

            /*早期开发用的测试代码，忽略*/
/*            List<string> list = new Parser().SliceText("#加法：123");
            foreach (var item in list) {
                Console.WriteLine(item);
            }*/

            //List<Expr> exprs = new Parser().Parse("如果 #甲=#0/#1+#2*#3 “ 呵呵 \\”呵”");

            /*            List<Expr> exprs = new Parser().Parse("1+2*3/4-5/6 ");
                        foreach (Expr e in exprs) {
                            Console.WriteLine(e.ToStr());
                        }

                        Console.WriteLine(Expr.EvalExprs(exprs, new Ctx()).ToStr());*/




            /*创建一个语境作为最顶层的库语境*/
            Ctx libCtx = new Ctx();
            /*在库语境中初始化标准库原生函数*/
            StdInit.InitStd(libCtx);
            /*在库语境中初始化扩展原生函数*/
            ExtInit.InitExt(libCtx);
            /*在库语境中执行初始化脚本*/
            Expr.EvalExprs(new Parser().Parse(InitScript.Script, libCtx), libCtx);
            /*把库语境放入全局Env*/
            Env.LibCtx = libCtx;

            /*创建一个语境作为用户语境，其父语境为库语境*/
            Ctx usrCtx = new Ctx(libCtx);
            /*把用户语境放入全局Env*/
            Env.UsrCtx = usrCtx;


            /*判断命令行传参，如果有参数，说明要执行对应的脚本文件*/
            if (args.Length > 0) {
                string fpath = args[0];
                if(!fpath.StartsWith("/") && !fpath.Contains(':')) {
                    fpath = Directory.GetCurrentDirectory() + '/' + fpath;
                }

                if (!File.Exists(fpath)) {
                    Console.WriteLine("要执行的文件不存在");
                } else {
                    FileInfo fi = new FileInfo(fpath);
                    string dir = fi.DirectoryName!;
                    Directory.SetCurrentDirectory(dir);
                    string script = File.ReadAllText(fpath);
                    Expr ans = Expr.EvalExprs(new Parser().Parse(script, usrCtx, fpath), usrCtx);
                    if(ans.Tp == TP.Err) {
                        ans.Echo();
                    }
                }
            } else {
                Console.WriteLine($"欢迎使用青语言V{Env.Version}——数心开物工作室");
            }

           
            string inpCode = "";
            /*从控制台读取用户输入，并执行*/
            while (true) {

                Console.Write(">> ");
                string? inp = Console.ReadLine();
                if (inp == null) {
                    continue;
                }

                /*如果本次输入是以~结尾，说明当前的输入还未完成，继续下一轮输入*/
                if (inp.Trim().EndsWith('~')) {
                    inpCode += inp.Trim().Substring(0, inp.Length-1);
                    continue;
                }

                inpCode += inp;

                try {
                    /*解析输入的代码并执行*/
                    List<Expr> exprs = new Parser().Parse(inpCode, usrCtx);
                    Expr ans = Expr.EvalExprs(exprs, usrCtx);
                    /*显示返回值*/
                    ans.Echo();
                } catch (Exception e) {
                    Console.WriteLine(e.Message);
                } finally {
                    inpCode = "";
                }

            }

        }
    }
}