﻿using Qing.Lang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Qing.Proto {


    class StrBuilder : Native {
        public override string Name { get; set; } = "创建缓冲字符串";
        public override string Desc { get; set; } = "无参，返回缓冲字符串对象；创建缓冲字符串，用于文本频繁修改";

        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj = null, List<Expr>? namedArgs = null) {
            return new Expr(TP.Obj, new StringBuilderObj());
        }

    }


    public class StringBuilderObj : Obj {

        public StringBuilderObj() {
            Raw = new StringBuilder();

            Map["@清空"] = new Expr(TP.Native, new Clear());
            Map["@转字符串"] = new Expr(TP.Native, new ToStr());
            Map["@追加"] = new Expr(TP.Native, new Append());
            Map["@插入"] = new Expr(TP.Native, new Insert());
            Map["@移除"] = new Expr(TP.Native, new Remove());

        }


        class Clear : Native {
            public override string Name { get; set; } = "清空";
            public override string Desc { get; set; } = "无参，返回逻辑；清空缓存";
            public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj = null, List<Expr>? namedArgs = null) {
                StringBuilder sb = (StringBuilder)obj!.Raw;
                
                if(sb.Length == 0) {
                    return new Expr(TP.Bool, false);
                } else {
                    sb.Clear();
                    return new Expr(TP.Bool, true);
                }
                
            }
        }

        class ToStr : Native {
            public override string Name { get; set; } = "转字符串";
            public override string Desc { get; set; } = "无参，返回字符串；缓存转为字符串";
            public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj = null, List<Expr>? namedArgs = null) {
                StringBuilder sb = (StringBuilder)obj!.Raw;

                return new Expr(TP.Str, sb.ToString());

            }
        }

        class Append : Native {
            public override string Name { get; set; } = "追加";
            public override string Desc { get; set; } = "参数1-字符串，可选参数2-逻辑，返回逻辑；追加字符串，若参数2为真，追加换行";
            public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj = null, List<Expr>? namedArgs = null) {
                if(args.Count == 0 || args[0].Tp != TP.Str) {
                    return Expr.Err("缓冲字符串@追加 方法参数错误");
                }

                StringBuilder sb = (StringBuilder)obj!.Raw;

                sb.Append(args[0].Str());

                if (args.Count > 1 && args[1].ToBool()) {
                    sb.Append("\n");
                }
                
                return new Expr(TP.Bool, true);
                

            }
        }

        class Insert : Native {
            public override string Name { get; set; } = "插入";
            public override string Desc { get; set; } = "参数1-整数，参数2-字符串，返回逻辑；在参数1指定的位置插入参数2对应的字符串";
            public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj = null, List<Expr>? namedArgs = null) {
                if (args.Count < 2 || args[0].Tp != TP.Int || args[1].Tp != TP.Str) {
                    return Expr.Err("缓冲字符串@插入 方法参数错误");
                }

                StringBuilder sb = (StringBuilder)obj!.Raw;

                if (args[0].Int() > sb.Length) {
                    return Expr.Err("缓冲字符串@插入 方法的位置参数越界");
                }

                sb.Insert(args[0].Int(), args[1].Str());

                return new Expr(TP.Bool, true);


            }
        }


        class Remove : Native {
            public override string Name { get; set; } = "移除";
            public override string Desc { get; set; } = "参数1-整数，参数2-整数，返回逻辑；在参数1指定的位置移除参数2指定长度的字符串";
            public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj = null, List<Expr>? namedArgs = null) {
                if (args.Count == 0 || args[0].Tp != TP.Int) {
                    return Expr.Err("缓冲字符串@移除 方法参数错误");
                }
                if(args.Count > 1 &&  (args[1].Tp != TP.Int || args[1].Int() <= 0)) {
                    return Expr.Err("缓冲字符串@移除 方法参数错误");
                }

                int len = -1;
                if (args.Count > 1) {
                    len = args[1].Int();
                }

                StringBuilder sb = (StringBuilder)obj!.Raw;

                if (args[0].Int() >= sb.Length) {
                    return Expr.Err("缓冲字符串@移除 方法的位置参数越界");
                }

                if(len <= 0) {
                    sb.Remove(args[0].Int(), sb.Length - args[0].Int());
                } else {
                    sb.Remove(args[0].Int(), args[1].Int());
                }
                

                return new Expr(TP.Bool, true);


            }
        }




    }
}
