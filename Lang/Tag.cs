﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qing.Lang {
    public class Tag {
        public Expr Name { get; set; } = Expr.NoneExpr;
        public List<Expr> Attrs { get; set; } = new List<Expr>();
        public Expr Children { get; set; } = Expr.NoneExpr;

    }
}
