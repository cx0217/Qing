﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qing.Lang {
    /*语言运行时的全局共享参数*/
    public class Env {

        /*当前版本*/
        public static string Version { get; set; } = "1.0";

        /*记录当前并行的任务数*/
        public static int Threads { get; set; } = 1;

        /*库语境*/
        public static Ctx? LibCtx { get; set; }

        /*用户语境*/
        public static Ctx? UsrCtx { get; set; }

        /*模块记录表*/
        public static Dictionary<string, Expr> ModuleMap { get; set; } = new Dictionary<string, Expr>();



        public static dynamic? ConsoleGUI { get; set; } = null;



        public static void ConsoleLog(string msg) {
            LibCtx!.GetNow("@显示").Native().Run(new List<Expr> { new Expr(TP.Str, msg) }, LibCtx);
        }

        public static void ConsoleLog(Expr expr) {
            LibCtx!.GetNow("@显示").Native().Run(new List<Expr> { expr }, LibCtx);
        }


        public static List<System.Timers.Timer> Timers { get; set; } = new List<System.Timers.Timer> ();

    }
}
