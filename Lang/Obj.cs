﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qing.Lang {
    /*
     * 对象类型的包装类
     * 本质上就是一个语境，继承了Ctx
     */
    public class Obj : Ctx {
        /*
         * 这里定义了一个raw，用来存放原生的对象
         * 如果需要封装C#原生的功能
         * 可以继承Obj类，把原始的对象放到raw里
         * 再添加对原始对象的操作方法
         * 相应的例子在Proto命名空间里
         */
        public object Raw { get; set; }

        public Obj() : base() {
            Raw = 0;
        }

        public Obj(Ctx? ctx) : this() {
            Father = ctx;
            Raw = 0;
        }
        public Obj(object raw) : this() {
            this.Raw = raw;
        }

        public virtual string toStr(string tp = "") {
            if (Map.Count == 0) return tp +"｛｝";
            string objStr = tp + "｛";
            foreach (var item in Map) {
                objStr += item.Key + '：' + item.Value.ToStr() + '，';
            }
            return objStr + '｝';
        }


    }


    


    public abstract class Prop {

        public dynamic obj;

        public Prop(Obj obj) {
            this.obj = obj;
        }


        public abstract Expr Qget(Ctx? ctx=null);
        public abstract Expr Qset(Expr val, Ctx? ctx = null);

    }

}
