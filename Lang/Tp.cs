﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qing.Lang {

    /*
     * 这里定义了Expr的类型
     * 
     */
    public enum TP {
        Err,    //异常类型
        None,   //空类型
        Bin,    //二进制类型
        Bool,   //逻辑类型
        Int,    //整数类型
        Float,  //小数类型
        Rune,   //字符类型,目前没有使用
        Str,    //字符串类型
        Obj,    //对象类型
        Arr,    //数组类型
        Tag,    //标签类型
        Var,    //变量类型
        Act,    //函数类型
        Dim,    //定义类型
        Path,   //路径类型
        Act_path,   //函数路径类型
        Call_act_path,  //函数调用路径类型
        Native,     //原生函数类型
        Func,       //自定义函数类型
        Paren,  //圆括号类型,用于提升优先级
        Block,      //中括号类型
        Brace,      //大括号类型
        Dim_func,   //定义函数类型 用于自定义函数
        Dim_obj,    //定义对象类型
        Call_act,   //函数调用类型
        Args,   //参数列表
        Prop,   //属性绑定
        Date,   //日期时间类型
        Task,   //任务类型
        Module, //模块类型
        Timer,  //定时器类型
        Word,   //单字类型 
        Op,     //中缀类型
        Meta,   //元类型

        Wait,   //等待语句
        Negate, //取反语句
        If,     //如果语句
        Elif,   //再则语句
        Else,   //否则语句
        While,  //当语句
        Until,  //执行...直到语句
        Foreach,    //遍历语句
        Return, //返回语句
        Throw,  //抛出语句
        Break,  //跳出语句
        Continue,   //继续语句
        Try,    //尝试语句
        Finally,    //例行语句
        Repeat,    //重复语句
        Match,  //匹配
        Default,    //默认

    }

    public class TPkit {

        /*将类型转换为相应的字符串表示,用于运行期获取变量类型*/
        public static string typeName(TP tp) {
            switch (tp) {
                case TP.None:
                    return "空类型";
                case TP.Err:
                    return "异常类型";
                case TP.Bool:
                    return "逻辑类型";
                case TP.Int:
                    return "整数类型";
                case TP.Float:
                    return "小数类型";
                case TP.Rune:
                    return "字符类型";
                case TP.Str:
                    return "字符串类型";
                case TP.Arr:
                    return "数组类型";
                case TP.Obj:
                    return "对象类型";
                case TP.Timer:
                    return "定时器类型";
                /*case TP.async:
                    return "异步函数类型";*/
                case TP.Task:
                    return "任务类型";
                case TP.Brace:
                    return "代码块类型";
                case TP.Tag:
                    return "标签类型";

                default:
                    return "表达式类型";
            }


        }
    }
}
