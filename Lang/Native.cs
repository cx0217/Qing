﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Qing.Lang {
    /*
     * 这是对语言原生函数的抽象类
     * Std里封装的原生函数都是实现了这个类
     */
    public abstract class Native {
        public Ctx? Ctx { get; set; } = null;
        public abstract string Name { get; set; }
        public abstract string Desc { get; set; }

        /*
         * 原生函数调用时调用的方法
         * 返回值是Expr
         * 参数列表是List<Expr>
         * 函数运行的父语境ctx
         * 对于原生封装的对象，通过obj传入
         */
        public abstract Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null);

    }


}
