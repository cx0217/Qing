﻿using Qing.Lang;
using Qing.Std;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qing.Dll {

    public class DllObj : Obj {


        public DllObj() :base() {
            Raw = new DateTime();

            Map["#动态库变量"] = new Expr(TP.Arr, 0, new List<Expr> { new Expr(TP.Int, 0), new Expr(TP.Str, "123")});
            Map["@三数和"] = new Expr(new ThreeSum());
            Map["@取小时"] = new Expr(new GetHour());
            Map["@设置小时"] = new Expr(new SetHour());
            Map["#小时"] = new Expr(TP.Prop, new BindRaw(this));
        }


        class ThreeSum : Native {
            public override string Name { get; set; } = "三数和";
            public override string Desc { get; set; } = "参数1-数字，参数2-数字，参数3-数字，返回数字；传入三个数字求和";
            public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
                return new Expr(TP.Int, args[0].Int() + args[1].Int() + args[2].Int());
            }


        }

        class GetHour : Native {
            public override string Name { get; set; } = "取小时";
            public override string Desc { get; set; } = "无参，返回小时数";
            public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
                DateTime dt = (DateTime)obj!.Raw;
                return new Expr(TP.Int, dt.Hour);
            }


        }

        class SetHour : Native {
            public override string Name { get; set; } = "设置小时";
            public override string Desc { get; set; } = "设置小时";
            public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
                DateTime dt = ((DateTime)obj!.Raw);
                string dtStr = dt.ToString("yyyy-MM-dd");
                dtStr += " " + args[0].Int().ToString();
                dtStr += dt.ToString(":mm:ss.fff");
                obj!.Raw = DateTime.Parse(dtStr);
                return new Expr(TP.Int, args[0]);

            }

        }


        class BindRaw : Prop {
            public BindRaw(Obj obj) : base(obj) {}

            public override Expr Qget(Ctx? ctx=null) {
                DateTime dt = (DateTime)obj!.Raw;
                return new Expr(TP.Int, dt.Hour);
            }

            public override Expr Qset(Expr val, Ctx? ctx=null) {
                DateTime dt = ((DateTime)obj!.Raw);
                string dtStr = dt.ToString("yyyy-MM-dd");
                dtStr += " " + val.Int().ToString();
                dtStr += dt.ToString(":mm:ss.fff");
                obj!.Raw = DateTime.Parse(dtStr);
                return val;
            }

        }


    }
}
