﻿using Qing.Lang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qing.Ext {
    public class ExtInit {
        public static void InitExt(Ctx ctx) {
            ctx.PutNow("@运行内置测试", new Expr(TP.Native, new RunInnerTest()));
            ctx.PutNow("@内置库数量", new Expr(TP.Native, new LibCtxLength()));
        }

    }
}
