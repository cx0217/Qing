﻿using Qing.Lang;
using Qing.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qing.Ext {

    class RunInnerTest : Native {
        public override string Name { get; set; } = "运行内置测试";
        public override string Desc { get; set; } = "1个逻辑类型的可选参数，返回任意；会运行内置的测试代码，如果传入的一个参数逻辑为真时会对本次测试计时，返回耗时";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            string script = TestScript.Script;
            Ctx tCx = new Ctx(Env.LibCtx); //创建专用的Ctx，避免污染用户Ctx

            if(args.Count > 0 && args[0].ToBool()) {
                script = "@显示、“测试用时 ” + @计时【元｛" + script + "｝】 + “秒”";             
            } 
            Expr ans = Expr.EvalExprs(new Parser().Parse(script, tCx), tCx);
            return ans;
        }

    }

    class LibCtxLength : Native {
        public override string Name { get; set; } = "内置库长度";
        public override string Desc { get; set; } = "无参，返回整数；获取内置的原生函数和对象的数量";
        public override Expr Run(List<Expr> args, Ctx ctx, Obj? obj=null, List<Expr>? namedArgs=null) {
            return new Expr(TP.Int, Env.LibCtx!.Map.Count);
        }

    }
}
